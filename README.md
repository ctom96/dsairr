# DSAIRR

DSAIRR - "Data Structures & Algorithms Reference Repo"


## Data Structures

The following Data Structures are implemented in this repo:

### Stack

Simple LIFO (Last-In, First-Out) data structure that is good for representing
tasks which are squeenced in time. I think a great example would be a simple
undo stack for an application like Visual Studio Code.

Elements are inserted, or  "pushed" to the top of the stack. Elements are
removed, or "popped" from the top as well.

#### Operations

* **Push**
	* Adds the element to the top of the stack
* **Pop**
  * Removes the topmost element from the stack and returns it
* **Empty**
  * Checks if the stack is empty
* **Peek**
  * Same as Pop, but it doesn't actually remove the item

### Queue

Simple FIFO (First-In, First-Out) data structure. Queues work best for 
representing data that is in-order and needs to be processed one at a time.

Elements are inerted into the rear of the queue, and removed from the front.

#### Operations

* **Add**
	* Adds the element to the back of the queue
* **Remove**
  * Removes the element from the front of the queue
* **Empty**
  * Checks if the queue is empty
* **Peek**
  * Same as Remove, but it doesn't actually remove the item

### Deque

A deque, or double-ended queue, is a queue which allows elements to be inserted
and removed from both the front and back. Basically, this is the combination of
the stack and queue, with additional, more-general application

Elements can be inserted and removed from both the front and rear, using the 
form insert(front/back) and remove(front/back).

#### Operations

* **insertFront**
	* Inserts an element in the front of the deque
* **insertBack**
	* Inserts an element in the back of the deque
* **removeFront**
	* Returns and removes the front-most element, or errors if one doesn't exist
* **removeBack**
	* Returns and removes the back-most element, or errors if one doesn't exist
* **Empty**
  * Returns true if the deque is emptyv
* **peekFront**
	* Returns the front-most element without removing it, or errors if one doesn't exist
* **peekBack**
	* Returns the back-most element without removing it, or errors if one doesn't exist

### Undirected Graph
### Directed Graph
### Tree
### Hash Table
