package queue

import (
	"testing"
)

func TestAdd(t *testing.T) {
	q := New()

	q.Add(1)
	if q.bottom.data != 1 {
		t.Errorf("Data inserted incorrectly")
	}

	q.Add(2)
	if q.bottom.data != 2 {
		t.Errorf("Data inserted incorrectly")
	}
	if q.top.data != 1 && q.top.next.data != 2 {
		t.Errorf("Data out of order")
	}
}

func TestRemove(t *testing.T) {
	q := New()
	val, err := q.Remove()

	// Initial conditions
	if val != nil || err == nil {
		t.Errorf("Remove returned a value in an empty queue")
	}
	if !q.Empty() {
		t.Errorf("Queue should still be empty after Remove")
	}

	q.Add(1)
	val, err = q.Remove()
	if err != nil {
		t.Errorf("Queue should have Removed without error")
	}
	if val != 1 {
		t.Errorf("Queue should have returned first-added value")
	}
	if !q.Empty() {
		t.Errorf("Queue should be empty after Remove with 1 element")
	}

	q.Add(1)
	q.Add(2)
	q.Add(3)

	val, err = q.Remove()
	if err != nil {
		t.Errorf("Queue should have Removeped without error")
	}
	if val != 1 {
		t.Errorf("Queue should have returned first-added value")
	}

	val, err = q.Remove()
	if err != nil {
		t.Errorf("Queue should have Removeped without error")
	}
	if val != 2 {
		t.Errorf("Queue should have returned first-added value")
	}

	val, err = q.Remove()
	if err != nil {
		t.Errorf("Queue should have Removeped without error")
	}
	if val != 3 {
		t.Errorf("Queue should have returned first-added value")
	}
}

func TestPeek(t *testing.T) {
	q := New()

	val, err := q.Peek()
	if val != nil || err == nil {
		t.Errorf("Peek should have errored out")
	}

	q.Add(1)
	val, err = q.Peek()
	if err != nil {
		t.Errorf("Queue should have peeked without error")
	}
	if val != 1 {
		t.Errorf("Incorrect value was peeked")
	}

	val, err = q.Peek()
	if err != nil {
		t.Errorf("Multiple peeks should not cause error")
	}
	if val != 1 {
		t.Errorf("Incorrect value was peeked")
	}
}

func TestEmpty(t *testing.T) {
	q := New()

	if !q.Empty() {
		t.Errorf("Empty failed when Queue was empty")
	}

	q.Add(1)
	if q.Empty() {
		t.Errorf("Empty failed when Queue had 1 element")
	}

	q.Remove()
	if !q.Empty() {
		t.Errorf("Queue should be empty after all elements removed")
	}
}
