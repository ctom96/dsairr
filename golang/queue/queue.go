package queue

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

// Queue is a thread-safe last-in last-out data structure
type Queue struct {
	mu     sync.Mutex
	top    *node
	bottom *node
	size   uint
}

// node is a specific instance of data in the queue
type node struct {
	data interface{}
	next *node
}

// New creates a new queue with basic defaults
func New() *Queue {
	return &Queue{
		size: 0,
	}
}

// Add enqueues the value to the end of the queue
func (q *Queue) Add(value interface{}) {
	q.mu.Lock()
	defer q.mu.Unlock()

	newNode := &node{data: value, next: nil}

	if q.bottom == nil {
		// first node insert
		q.bottom = newNode
		q.top = newNode
		q.size++
		return
	}

	q.bottom.next = newNode
	q.bottom = newNode
	q.size++

}

// Remove dequeues the value at the beginning of the queue and returns it
// If the queue is empty, returns an error
func (q *Queue) Remove() (interface{}, error) {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.Empty() {
		return nil, errors.New("Queue is empty")
	}

	val := q.top.data
	q.top = q.top.next
	q.size--

	// Cleanup when queue is emptied
	if q.size == 0 {
		q.top = nil
		q.bottom = nil
	}

	return val, nil
}

// Empty returns true if the queue is empty, false otherwise
func (q *Queue) Empty() bool {
	return q.size == 0
}

// Peek looks at the next element that will be dequeued with Remove
// and returns it without removing it. If there are no elements, it
// returns an error
func (q *Queue) Peek() (interface{}, error) {
	q.mu.Lock()
	defer q.mu.Unlock()

	if q.Empty() {
		return nil, errors.New("Queue is empty")
	}

	return q.top.data, nil
}

// Print returns a string representation of the queue
func (q *Queue) Print() string {
	q.mu.Lock()
	defer q.mu.Unlock()

	var retString strings.Builder
	fmt.Fprintf(&retString, "Top -> ")

	curNode := q.top
	for curNode != nil {
		if curNode.next != nil {
			fmt.Fprintf(&retString, "[%v] -> ", curNode.data)
		} else {
			fmt.Fprintf(&retString, "[%v]", curNode.data)

		}
		curNode = curNode.next
	}

	fmt.Fprintf(&retString, " <- Bottom")
	return retString.String()
}
