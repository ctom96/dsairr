package stack

import (
	"errors"
	"sync"
)

// Stack is a thread-safe instance of a last-in first-out data structure
type Stack struct {
	mu   sync.Mutex
	top  *node
	size uint
}

// node is a specific instance of data in the stack
// data is the actual payload of data
// next points to the next node on the stack
type node struct {
	data interface{}
	next *node
}

// New creates a new stack object
func New() *Stack {
	return &Stack{
		top:  &node{data: nil, next: nil},
		size: 0,
	}
}

// Empty returns true if this stack is empty, false otherwise
func (s *Stack) Empty() bool {
	return s.size == 0
}

// Pop gets the current item at the top of the stack, returns it,
// and removes it from the stack. If the stack is empty, it returns
// an error.
func (s *Stack) Pop() (interface{}, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.Empty() {
		return nil, errors.New("Stack is empty")
	}

	val := s.top.data
	s.top = s.top.next
	s.size--

	return val, nil
}

// Push adds a payload {value} to the top of the stack
func (s *Stack) Push(value interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()

	newNode := &node{
		data: value,
		next: s.top,
	}
	s.top = newNode
	s.size++
}

// Peek returns the item at the top of the stack without
// actually removing it. If the stack is empty, it returns
// an error.
func (s *Stack) Peek() (interface{}, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.Empty() {
		return nil, errors.New("Stack is empty")
	}

	return s.top.data, nil
}
