package stack

import (
	"testing"
)

func TestPush(t *testing.T) {
	testStack := New()
	if testStack.top == nil {
		t.Errorf("Top uninitialized")
	}
	if testStack.top.data != nil {
		t.Errorf("Data not initialized to nil")
	}

	testStack.Push(1)
	if testStack.top.data != 1 {
		t.Errorf("Data inserted incorrectly")
	}

	testStack.Push(2)
	if testStack.top.data != 2 {
		t.Errorf("Data inserted incorrectly")
	}
	if testStack.top.next.data != 1 {
		t.Errorf("Insert order is incorrect")
	}
}

func TestPop(t *testing.T) {
	testStack := New()
	val, err := testStack.Pop()

	// Initial conditions
	if val != nil || err == nil {
		t.Errorf("Pop returned a value in an empty stack")
	}
	if !testStack.Empty() {
		t.Errorf("Stack should still be empty after pop")
	}

	testStack.Push(1)
	val, err = testStack.Pop()
	if err != nil {
		t.Errorf("Stack should have popped without error")
	}
	if val != 1 {
		t.Errorf("Stack should have returned most-recently added value")
	}
	if !testStack.Empty() {
		t.Errorf("Stack should be empty after pop with 1 element")
	}

	testStack.Push(1)
	testStack.Push(2)
	testStack.Push(3)

	val, err = testStack.Pop()
	if err != nil {
		t.Errorf("Stack should have popped without error")
	}
	if val != 3 {
		t.Errorf("Stack should have returned most-recently added value")
	}

	val, err = testStack.Pop()
	if err != nil {
		t.Errorf("Stack should have popped without error")
	}
	if val != 2 {
		t.Errorf("Stack should have returned most-recently added value")
	}

	val, err = testStack.Pop()
	if err != nil {
		t.Errorf("Stack should have popped without error")
	}
	if val != 1 {
		t.Errorf("Stack should have returned most-recently added value")
	}
}

func TestPeek(t *testing.T) {
	testStack := New()

	val, err := testStack.Peek()
	if val != nil || err == nil {
		t.Errorf("Peek should have errored out")
	}

	testStack.Push(1)
	val, err = testStack.Peek()
	if err != nil {
		t.Errorf("Stack should have peeked without error")
	}
	if val != 1 {
		t.Errorf("Incorrect value was peeked")
	}

	val, err = testStack.Peek()
	if err != nil {
		t.Errorf("Multiple peeks should not cause error")
	}
	if val != 1 {
		t.Errorf("Incorrect value was peeked")
	}
}

func TestEmpty(t *testing.T) {
	testStack := New()

	if !testStack.Empty() {
		t.Errorf("Empty failed when stack was empty")
	}

	testStack.Push(1)
	if testStack.Empty() {
		t.Errorf("Empty failed when stack had 1 element")
	}
}
