package deque

import (
	"testing"
)

func TestInsertFront(t *testing.T) {
	d := New()

	d.InsertFront(1)
	if d.size != 1 {
		t.Errorf("Deque size should have been 1 after 1 front insert")
	}
	if d.front.data != 1 {
		t.Errorf("Deque front value should have been 1")
	}
	if d.back.data != d.front.data {
		t.Errorf("Deque back value should equal deque front value after 1 insertion")
	}

	d.InsertFront(2)
	if d.size != 2 {
		t.Errorf("Deque size should have been 2 after 2 front insert")
	}
	if d.front.data != 2 {
		t.Errorf("Deque front value should have been 2")
	}
	if d.back.data != 1 {
		t.Errorf("Deque back value should have been 1")
	}
	// Check for proper pointers
	if d.back.next.data != 2 {
		t.Errorf("Deque back's next should be the current front after 2 InsertFronts")
	}
	if d.front.prev.data != 1 {
		t.Errorf("Deque front's previous should be the current back after 2 InsertFronts")
	}

	d.InsertFront(3)
	if d.size != 3 {
		t.Errorf("Deque size should have been 3 after 3 front insert")
	}
	if d.front.data != 3 {
		t.Errorf("Deque front value should have been 3")
	}
	if d.back.data != 1 {
		t.Errorf("Deque back value should have been 1")
	}
	// Check for proper pointers
	if d.back.next.data != 2 {
		t.Errorf("Deque back's next should be the middle value after 3 InsertFronts. Got %d", d.back.next.data)
	}
	if d.front.prev.data != 2 {
		t.Errorf("Deque front's previous should be the middle value after 3 InsertFronts. Got %d", d.back.next.data)
	}
}

func TestInsertBack(t *testing.T) {
	d := New()

	d.InsertBack(1)
	if d.size != 1 {
		t.Errorf("Deque size should have been 1 after 1 front insert")
	}
	if d.back.data != 1 {
		t.Errorf("Deque back value should have been 1")
	}
	if d.back.data != d.front.data {
		t.Errorf("Deque front value should equal deque back value after 1 insertion")
	}

	d.InsertBack(2)
	if d.size != 2 {
		t.Errorf("Deque size should have been 2 after 2 front insert")
	}
	if d.front.data != 1 {
		t.Errorf("Deque front value should have been 1")
	}
	if d.back.data != 2 {
		t.Errorf("Deque back value should have been 2")
	}
	// Check for proper pointers
	if d.back.next.data != 1 {
		t.Errorf("Deque back's next should be the current front after 2 InsertBacks")
	}
	if d.front.prev.data != 2 {
		t.Errorf("Deque front's previous should be the current back after 2 InsertBacks")
	}

	d.InsertBack(3)
	if d.size != 3 {
		t.Errorf("Deque size should have been 3 after 3 front insert")
	}
	if d.back.data != 3 {
		t.Errorf("Deque back value should have been 3")
	}
	if d.front.data != 1 {
		t.Errorf("Deque front value should have been 1")
	}
	// Check for proper pointers
	if d.back.next.data != 2 {
		t.Errorf("Deque back's next should be the middle value after 3 InsertBacks. Got %d", d.back.next.data)
	}
	if d.front.prev.data != 2 {
		t.Errorf("Deque front's previous should be the middle value after 3 InsertBacks. Got %d", d.back.next.data)
	}
}

func TestPeekFront(t *testing.T) {
	d := New()

	val, err := d.PeekFront()
	if val != nil || err == nil {
		t.Errorf("Val should be nil and err should not be nil on peeking empty deque")
	}

	d.InsertFront(1)
	d.InsertFront(2)
	d.InsertFront(3)
	// Current deque: [1, 2, 3]

	val, err = d.PeekFront()
	if val != 3 && err != nil {
		t.Errorf("Val should have been the last inserted, err should be nil")
	}

	// Should have no effect
	d.InsertBack(3)

	val, err = d.PeekFront()
	if val != 3 && err != nil {
		t.Errorf("Val should have been the last inserted, err should be nil")
	}
}

func TestPeekBack(t *testing.T) {
	d := New()

	val, err := d.PeekBack()
	if val != nil || err == nil {
		t.Errorf("Val should be nil and err should not be nil on peeking empty deque")
	}

	d.InsertBack(1)
	d.InsertBack(2)
	d.InsertBack(3)
	// Current deque: [1, 2, 3]

	val, err = d.PeekBack()
	if val != 3 && err != nil {
		t.Errorf("Val should have been the last inserted, err should be nil")
	}

	// Should have no effect
	d.InsertFront(3)

	val, err = d.PeekBack()
	if val != 3 && err != nil {
		t.Errorf("Val should have been the last inserted, err should be nil")
	}
}

func TestRemoveFront(t *testing.T) {
	d := New()

	val, err := d.RemoveFront()
	if val != nil || err == nil {
		t.Errorf("Val should be nil and err should not be nil on removing from an empty deque")
	}

	d.InsertFront(1)
	d.InsertFront(2)
	d.InsertFront(3)
	// Current deque: [1, 2, 3]

	val, err = d.RemoveFront()
	if val != 3 && err != nil {
		t.Errorf("Should have returned the front item. err should be nil")
	}
	if d.front.data != 2 {
		t.Errorf("front should now be 2")
	}
	if d.size != 2 {
		t.Errorf("deque should have 2 items")
	}

	val, err = d.RemoveFront()
	if val != 2 && err != nil {
		t.Errorf("Should have returned the front item. err should be nil")
	}
	if d.front.data != 1 {
		t.Errorf("front should now be 1")
	}
	if d.size != 1 {
		t.Errorf("deque should have 1 item")
	}

	d.InsertBack(4)
	val, err = d.RemoveFront()
	if val != 1 && err != nil {
		t.Errorf("Should have returned the front item. err should be nil")
	}
	if d.front.data != 4 {
		t.Errorf("front should now be 4")
	}
	if d.size != 1 {
		t.Errorf("deque should have 1 item")
	}

	// empties the deque
	val, err = d.RemoveFront()
	if val != 4 && err != nil {
		t.Errorf("Should have returned the front item. err should be nil")
	}
	if d.front != nil {
		t.Errorf("front should now be nil")
	}
	if d.size != 0 {
		t.Errorf("deque should be empty")
	}
}
func TestRemoveBack(t *testing.T) {
	d := New()

	val, err := d.RemoveBack()
	if val != nil || err == nil {
		t.Errorf("Val should be nil and err should not be nil on removing from an empty deque")
	}

	d.InsertBack(1)
	d.InsertBack(2)
	d.InsertBack(3)
	// Current deque: [1, 2, 3]

	val, err = d.RemoveBack()
	if val != 1 && err != nil {
		t.Errorf("Should have returned the Back item. err should be nil")
	}
	if d.back.data != 2 {
		t.Errorf("Back should now be 2")
	}
	if d.size != 2 {
		t.Errorf("deque should have 2 items")
	}

	val, err = d.RemoveBack()
	if val != 2 && err != nil {
		t.Errorf("Should have returned the Back item. err should be nil")
	}
	if d.back.data != 1 {
		t.Errorf("Back should now be 1")
	}
	if d.size != 1 {
		t.Errorf("deque should have 1 item")
	}

	d.InsertFront(4)
	val, err = d.RemoveBack()
	if val != 1 && err != nil {
		t.Errorf("Should have returned the Back item. err should be nil")
	}
	if d.front.data != 4 {
		t.Errorf("Front should now be 4")
	}
	if d.size != 1 {
		t.Errorf("deque should have 1 item")
	}

	// empties the deque
	val, err = d.RemoveBack()
	if val != 4 && err != nil {
		t.Errorf("Should have returned the Back item. err should be nil")
	}
	if d.back != nil {
		t.Errorf("Back should now be nil")
	}
	if d.size != 0 {
		t.Errorf("deque should be empty")
	}
}

func TestEmpty(t *testing.T) {
	d := New()

	if !d.Empty() {
		t.Errorf("deque should be empty on creation")
	}

	d.InsertFront(1)
	if d.Empty() {
		t.Errorf("deque should not be empty after 1 insert")
	}

	d.RemoveBack()
	if !d.Empty() {
		t.Errorf("deque should be empty on creation")
	}
}
