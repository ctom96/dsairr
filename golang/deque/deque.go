package deque

import (
	"errors"
	"sync"
)

// Deque is a thread-safe general-list data structure
type Deque struct {
	mu    sync.Mutex
	front *node
	back  *node
	size  uint
}

// node is a specific instance of data in the deque
type node struct {
	data interface{}
	next *node
	prev *node
}

// New creates a new Deque and returns its pointer
func New() *Deque {
	return &Deque{
		mu:   sync.Mutex{},
		size: 0,
	}
}

// InsertFront adds value to the front of the deque
func (d *Deque) InsertFront(value interface{}) {
	d.mu.Lock()
	defer d.mu.Unlock()

	if d.front == nil && d.back == nil {
		// this was an empty deque
		d.insertFirstNode(value)
		return
	}

	newNode := &node{
		data: value,
		next: nil,
		prev: d.front,
	}

	d.front.next = newNode
	d.front = newNode
	d.size++

}

// InsertFront adds value to the back of the deque
func (d *Deque) InsertBack(value interface{}) {
	d.mu.Lock()
	defer d.mu.Unlock()

	if d.front == nil && d.back == nil {
		// this was an empty deque
		d.insertFirstNode(value)
		return
	}

	newNode := &node{
		data: value,
		next: d.back,
		prev: nil,
	}

	d.back.prev = newNode
	d.back = newNode
	d.size++
}

// insertFirstNode is the special case where the deque was empty
func (d *Deque) insertFirstNode(value interface{}) {
	// No need to lock mutex, it has already been locked from the Intert methods
	newNode := &node{
		data: value,
		next: nil,
		prev: nil,
	}
	d.front = newNode
	d.back = newNode
	d.size++
}

// RemoveFront gets the value at the front of the deque, removes it, and returns it.
// If the deque is empty, RemoveFront returns nil and an error
func (d *Deque) RemoveFront() (interface{}, error) {
	if d.size == 0 {
		return nil, errors.New("deque was empty")
	}

	val := d.front.data
	if d.front.prev != nil {
		d.front.prev.next = nil
	}
	d.front = d.front.prev
	d.size--

	return val, nil
}

// RemoveBack gets the value at the back of the deque, removes it, and returns it.
// If the deque is empty, RemoveBack returns nil and an error
func (d *Deque) RemoveBack() (interface{}, error) {
	if d.size == 0 {
		return nil, errors.New("deque was empty")
	}

	val := d.back.data
	if d.back.next != nil {
		d.back.next.prev = nil
	}
	d.back = d.back.next
	d.size--

	return val, nil
}

// Empty returns true when the deque is empty
func (d *Deque) Empty() bool {
	d.mu.Lock()
	defer d.mu.Unlock()

	return d.size == 0
}

// PeekFront will get the value at the front of the deque without
// removing it. If the deque is empty, PeekFront will return nil
// and an error
func (d *Deque) PeekFront() (interface{}, error) {
	if d.size == 0 {
		return nil, errors.New("deque was empty")
	}

	return d.front.data, nil
}

// PeekBack will get the value at the back of the deque without
// removing it. If the deque is empty, PeekBack will return nil
// and an error
func (d *Deque) PeekBack() (interface{}, error) {
	if d.size == 0 {
		return nil, errors.New("deque was empty")
	}

	return d.back.data, nil
}
